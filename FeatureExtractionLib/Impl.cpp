

#include "IBlobProcessor.h"
#include "FeatureExtraction.h"
#include "RadialFunctions.h"

#define FEATURE_DLL_EXPORTS

using namespace fe;
using namespace cv;
using namespace std;



class BlobProcessor : public fe::IBlobProcessor
{
public:
	BlobProcessor();
	~BlobProcessor();

	virtual std::string GetType()
	{
		return "Simple Blob Processor by Max Korolev";
	}

	virtual std::vector<cv::Mat> DetectBlobs(cv::Mat image)
	{
		std::vector<cv::Mat> res;
		cv::Mat binary(image.rows, image.cols, CV_8UC1);
		cv::threshold(image, binary, 127, 255, CV_THRESH_BINARY_INV);
		cv::imshow("binary", binary);
		std::vector <std::vector<cv::Point>> contours;
		std::vector <cv::Vec4i> hierarchy;
		cv::findContours(binary, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE);
		cv::Mat drawing = cv::Mat::zeros(binary.size(), CV_8UC3);
		cv::RNG rng(0xFFFFFFFF);

		for (int idx = 0; idx >= 0; idx = hierarchy[idx][0])
		{
			cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			cv::drawContours(drawing, contours, idx, color, CV_FILLED, cv::LineTypes::LINE_8, hierarchy);
		}
		cv::imshow("drawing", drawing);

		for (int idx = 0; idx >= 0; idx = hierarchy[idx][0])
		{
			cv::Point2f center;
			float radius;
			cv::minEnclosingCircle(contours[idx], center, radius);
			res.push_back(cv::Mat::zeros(round((double)radius) * 2, round((double)radius) * 2, CV_8UC1));
			cv::Scalar white = cv::Scalar(255, 255, 255);
			cv::Point2f offset = -center + cv::Point2f(radius, radius);
			cv::drawContours(res.back(), contours, idx, white, CV_FILLED, cv::LineTypes::LINE_8, hierarchy, 255, offset);
			cv::imshow("symbol", res.back());
		}
		return res;
	}

	virtual std::vector<cv::Mat> NormalizeBlobs(std::vector<cv::Mat> & blobs, int side)
	{
		std::vector<cv::Mat> normalized;
		for (auto& blob : blobs)
		{
			cv::Mat m;
			cv::resize(blob, m, cv::Size(side, side));
			normalized.push_back(m);
			cv::imshow("normalized", m);
		}
		return normalized;

	}

private:

};

BlobProcessor::BlobProcessor()
{
}

BlobProcessor::~BlobProcessor()
{
}

std::shared_ptr <fe::IBlobProcessor> fe::CreateBlobProcessor(){
	return std::make_shared<BlobProcessor>();
}

class ChebPolynomialManager : public fe::PolynomialManager
{
	virtual void InitBasis(int n_max, int diameter)
	{
		//polynomials.resize(n_max);
		cv::Mat matRe = cv::Mat::zeros(diameter, diameter, CV_64FC1);
		cv::Mat matIm = cv::Mat::zeros(diameter, diameter, CV_64FC1);
		double * vectRe = matRe.ptr<double>();
		double * vectIm = matIm.ptr<double>();

		for (int n = 0; n <= n_max; n++)
		{
			polynomials.emplace_back();
			for (int m = 0; m <= n_max; m++){
				for (int i = 0; i < diameter; i++)
				{
					for (int j = 0; j < diameter; j++){
						double x = (j - diameter / 2.0) / (diameter / 2.0);
						double y = -(i - diameter / 2.0) / (diameter / 2.0);
						//double radial = rf::RadialFunctions::ShiftedLegendre(sqrt(y*y + x*x), n);
						double radial = rf::RadialFunctions::ShiftedChebyshev(sqrt(y*y + x*x), n);
						double fi = atan2(y, x);
						if (abs(x) < 1e-3 && abs(y) < 1e-3) fi = 0;
						vectRe[i*diameter + j] = radial * cos(m*fi);
						vectIm[i*diameter + j] = radial * sin(m*fi);
					}
				}
				polynomials[n].push_back({ matRe.clone(), matIm.clone() });
			}
		}
	}

	virtual fe::ComplexMoments Decompose(cv::Mat blob)
	{
		cv::Mat blob_doub = cv::Mat::zeros(blob.rows, blob.cols, CV_64FC1);
		blob.convertTo(blob_doub, CV_64FC1, 1.0 / 255.0 / (double)blob.rows / (double)blob.cols);
		int polynomials_count = 0;
		for (auto& polynom_line : polynomials)
		{
			polynomials_count += polynom_line.size();
		}
		fe::ComplexMoments dec;
		dec.abs = cv::Mat::zeros(cv::Size(polynomials_count, 1), CV_64FC1);
		dec.phase = cv::Mat::zeros(cv::Size(polynomials_count, 1), CV_64FC1);
		dec.re = cv::Mat::zeros(cv::Size(polynomials_count, 1), CV_64FC1);
		dec.im = cv::Mat::zeros(cv::Size(polynomials_count, 1), CV_64FC1);

		double * re = dec.re.ptr<double>();
		double * im = dec.im.ptr<double>();
		double * abs = dec.abs.ptr<double>();
		double * phase = dec.phase.ptr<double>();

		size_t idx = 0;
		for (auto &pl : polynomials)
		{
			for (auto &p : pl)
			{
				re[idx] = sum(blob_doub.mul(p.first))[0];
				im[idx] = -sum(blob_doub.mul(p.second))[0];
				abs[idx] = sqrt(re[idx] * re[idx] + im[idx] * im[idx]);
				phase[idx] = atan2(im[idx], re[idx]);
				idx++;
			}
		}
		double norm = cv::norm(dec.abs);
		dec.abs /= norm;
		dec.re /= norm;
		dec.im /= norm;

		return dec;
	}

	virtual cv::Mat Recovery(fe::ComplexMoments & decomposition)
	{
		int size = polynomials[0][0].first.cols;
		cv::Mat rec = cv::Mat::zeros(size, size, CV_64FC1);
		int index = 0;
		for (auto& polynom_line : polynomials)
		{
			for (auto& polynom : polynom_line)
			{
				rec += decomposition.re.ptr<double>()[index] * polynom.first -
					decomposition.im.ptr<double>()[index] * polynom.second;
				index++;
			} 
		}
		return rec;
	}

	virtual std::string GetType()
	{
		return "Polynomial";
	}
};

std::shared_ptr <fe::PolynomialManager> fe::CreatePolynomialManager()
{
	return std::make_shared<ChebPolynomialManager>();
}