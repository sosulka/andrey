#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	vector <size_t> conf = { 2, 3, 3, 1 };
	auto nn = CreateNeuralNetwork(conf);
	cout << nn->GetType().c_str() << endl;
	vector < vector <float> > inputs, outputs;
	bool success = LoadData("xor.data", inputs, outputs);
	if (!success)
	{
		cout << "could not open file \"xor.data\"" << endl;
		return -1;
	}

	cout << "Final error: " << BackPropTraining(nn, inputs, outputs, 10000, 1.e-2, 0.2, true) << endl;
	nn->Save("xor.ann");
	return 0;
}