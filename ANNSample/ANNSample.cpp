#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;
	vector < vector<float>> inputs, outputs;
	bool success = LoadData("xor.data", inputs, outputs);
	auto pAnn = CreateNeuralNetwork();
	cout << pAnn->GetType() << endl;
	pAnn->Load("xor.ann");
	for (int i = 0; i < inputs.size(); i++)
	{
		auto res = pAnn->Predict(inputs[i]);
		cout << inputs[i][0] << "\t" << inputs[i][1] << "\t" << res[0] << "\t" << outputs[i][0] << endl;
	}

	return 0;
}