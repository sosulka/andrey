#define ANNDLL_EXPORTS
#include <ANN.h>
#include <fstream>
#include <iomanip>
#include <iostream>

using namespace std;

bool ANN::ANeuralNetwork::Load(std::string filepath)
{
	std::ifstream file(filepath);
	if (!file.is_open()) return false;
	int buffer;
	const int CHAR_BUF_LEN = 100;
	char char_buffer[CHAR_BUF_LEN];
	file.getline(char_buffer, CHAR_BUF_LEN);
	std::string string_buffer = std::string(char_buffer);
	memset(char_buffer, 0, CHAR_BUF_LEN);
	if (string_buffer != std::string("activation type:"))
		throw "incorrect file format";
	file >> buffer;
	activation_type = (ActivationType)buffer;
	file.getline(char_buffer, CHAR_BUF_LEN);
	file.getline(char_buffer, CHAR_BUF_LEN);
	string_buffer = std::string(char_buffer);
	memset(char_buffer, 0, CHAR_BUF_LEN);
	if (string_buffer != std::string("activation scale:"))
		throw "incorrect file format";
	file >> scale;
	file.getline(char_buffer, CHAR_BUF_LEN);
	file.getline(char_buffer, CHAR_BUF_LEN);
	string_buffer = std::string(char_buffer);
	memset(char_buffer, 0, CHAR_BUF_LEN);
	if (string_buffer != std::string("configuration:"))
		throw "incorrect file format";
	file >> buffer;
	configuration.resize(buffer);
	for (size_t i = 0; i < configuration.size(); i++) {
		file >> configuration[i];
	}
	file.getline(char_buffer, CHAR_BUF_LEN);
	file.getline(char_buffer, CHAR_BUF_LEN);
	string_buffer = std::string(char_buffer);
	memset(char_buffer, 0, CHAR_BUF_LEN);
	if (string_buffer != std::string("weights:"))
		throw "incorrect file format";
	weights.resize(configuration.size() - 1);
	for (size_t layer_idx = 0; layer_idx < weights.size(); layer_idx++) {
		weights[layer_idx].resize(configuration[layer_idx]);
		for (size_t from_idx = 0; from_idx < weights[layer_idx].size(); from_idx++) {
			weights[layer_idx][from_idx].resize(configuration[layer_idx + 1]);
			for (size_t to_idx = 0; to_idx < weights[layer_idx][from_idx].size(); to_idx++) {
				file >> weights[layer_idx][from_idx][to_idx];
			}
		}
	}
	file.close();
	is_trained = true;
	return true;
}

bool ANN::ANeuralNetwork::Save(std::string filepath)
{
	if (!is_trained) return false;
	std::ofstream file(filepath);
	if (!file.is_open()) return false;
	file << std::setprecision(9);
	file << "activation type:" << std::endl;
	file << (int)activation_type << std::endl;
	file << "activation scale:" << std::endl;
	file << scale << std::endl;
	file << "configuration:" << std::endl;
	file << configuration.size() << "\t";
	for each (int neuron_count in configuration) {
		file << neuron_count << "\t";
	}
	file << std::endl << "weights:" << std::endl;
	for each (auto weight_matrix in weights) {
		for each (auto weight_line in weight_matrix) {
			for each (auto weight in weight_line) {
				file << weight << " ";
			}
			file << std::endl;
		}
	}
	file.close();
	return true;
}

bool ANN::LoadData(
	std::string filepath,
	std::vector<std::vector<float>> & inputs,
	std::vector<std::vector<float>> & outputs)
{
	std::ifstream file(filepath);
	if (!file.is_open()) return false;
	const int CHAR_BUF_LEN = 100;
	char char_buffer[CHAR_BUF_LEN];
	file.getline(char_buffer, CHAR_BUF_LEN);
	std::string string_buffer = std::string(char_buffer);
	memset(char_buffer, 0, CHAR_BUF_LEN);
	if (string_buffer != std::string("input_count:"))
		throw "incorrect file format";
	int input_count;
	file >> input_count;
	file.getline(char_buffer, CHAR_BUF_LEN);
	file.getline(char_buffer, CHAR_BUF_LEN);
	string_buffer = std::string(char_buffer);
	memset(char_buffer, 0, CHAR_BUF_LEN);
	if (string_buffer != std::string("output_count:"))
		throw "incorrect file format";
	int output_count;
	file >> output_count;
	file.getline(char_buffer, CHAR_BUF_LEN);
	file.getline(char_buffer, CHAR_BUF_LEN);
	string_buffer = std::string(char_buffer);
	memset(char_buffer, 0, CHAR_BUF_LEN);
	if (string_buffer != std::string("primer_count:"))
		throw "incorrect file format";
	int primer_count;
	file >> primer_count;
	file.getline(char_buffer, CHAR_BUF_LEN);
	file.getline(char_buffer, CHAR_BUF_LEN);
	string_buffer = std::string(char_buffer);
	memset(char_buffer, 0, CHAR_BUF_LEN);
	if (string_buffer != std::string("data:"))
		throw "incorrect file format";
	inputs.resize(primer_count);
	outputs.resize(primer_count);
	//���� �� ��������
	for (int i = 0; i < primer_count; i++) {
		inputs[i].resize(input_count);
		//��������� �����
		for (int j = 0; j < input_count; j++) {
			file >> inputs[i][j];
		}
		file.getline(char_buffer, CHAR_BUF_LEN);
		//��������� ������
		outputs[i].resize(output_count);
		for (int j = 0; j < output_count; j++) {
			file >> outputs[i][j];
		}
		file.getline(char_buffer, CHAR_BUF_LEN);
		file.getline(char_buffer, CHAR_BUF_LEN);
	}
	file.close();
	return true;
}

bool ANN::SaveData(
	std::string filepath,
	std::vector<std::vector<float>> & inputs,
	std::vector<std::vector<float>> & outputs)
{
	if (inputs.size() != outputs.size())
		throw "input size and output size must be the same";
	if (inputs.size() * outputs.size() == 0)
		throw "empty data";
	size_t input_count = inputs[0].size();
	size_t output_count = outputs[0].size();
	for (size_t i = 0; i < inputs.size(); i++) {
		if (inputs[i].size() != input_count)
			throw "incorrect input size";
		if (outputs[i].size() != output_count)
			throw "incorrect output size";
	}
	std::ofstream file(filepath);
	if (!file.is_open()) return false;
	file << std::setprecision(9);
	file << "input_count:" << std::endl;
	file << inputs[0].size() << std::endl;
	file << "output_count:" << std::endl;
	file << outputs[0].size() << std::endl;
	file << "primer_count:" << std::endl;
	file << inputs.size() << std::endl;
	file << "data:" << std::endl;
	for (size_t i = 0; i < inputs.size(); i++) {
		for (size_t j = 0; j < input_count; j++) {
			file << inputs[i][j] << "\t";
		}
		file << std::endl;
		for (size_t j = 0; j < output_count; j++) {
			file << outputs[i][j] << "\t";
		}
		file << std::endl;
		file << std::endl;
	}
	file.close();
	return true;
}

std::vector<size_t> ANN::ANeuralNetwork::GetConfiguration()
{
	return configuration;
}

float ANN::ANeuralNetwork::Activation(float neuronInput)
{
	if (activation_type == POSITIVE_SYGMOID) {
		return (1.f / (1.f + expf(-scale * neuronInput)));
	}
	else if (activation_type == BIPOLAR_SYGMOID) {
		return (2.f / (1.f + expf(-scale * neuronInput)) - 1.f);
	}
	return -1.f;
}

float ANN::ANeuralNetwork::ActivationDerivative(float activation)
{
	if (activation_type == POSITIVE_SYGMOID) {
		return scale * activation * (1.f - activation);
	}
	else if (activation_type == BIPOLAR_SYGMOID) {
		return scale * 0.5f * (1.f + activation) * (1.f - activation);
	}
	return -1;
}

ANN::ANeuralNetwork::~ANeuralNetwork()
{}

std::string ANN::GetTestString()
{
	return "You succesfully plug ANN library!";
}

void ANN::ANeuralNetwork::RandomInit()
{
	weights.resize(configuration.size() - 1);
	for (unsigned int layer_index = 0; layer_index < configuration.size() - 1; layer_index++) {
		weights[layer_index].resize(configuration[layer_index]);
		for (unsigned int from_index = 0; from_index < weights[layer_index].size(); from_index++) {
			weights[layer_index][from_index].resize(configuration[layer_index + 1]);
			for (unsigned int to_index = 0; to_index < weights[layer_index][from_index].size(); to_index++) {
				weights[layer_index][from_index][to_index] = 2.f * (rand() / float(RAND_MAX) - 0.5f);
			}
		}
	}
}
class NeuralNetworkImpl : public ANN::ANeuralNetwork
{
public:
	NeuralNetworkImpl(std::vector<size_t>conf, ANeuralNetwork::ActivationType aType, float scale);
	virtual std::string GetType();
	virtual std::vector <float> Predict(std::vector<float> & input);
};
NeuralNetworkImpl::NeuralNetworkImpl(std::vector<size_t>conf, NeuralNetworkImpl::ActivationType aType, float scale)
{
	this->configuration = conf;
	this->activation_type = aType;
	this->scale = scale;
	if (!conf.empty())
	{
		RandomInit();
	}
}
std::string NeuralNetworkImpl::GetType()
{
	return "shfsdvfsgvhxdf";
}
std::vector<float> NeuralNetworkImpl::Predict(std::vector<float> & input)
{
	//throw std::runtime_error("Not implimented");
	if (!is_trained) throw std::runtime_error("Not implimented");// ������� �� ����
	if (configuration.empty()) throw std::runtime_error("Not implimented");
	if (input.size() != configuration[0]) throw std::runtime_error("Not implimented"); //��������� ����������� ������� ������

	std::vector<float> prev_out = input;
	std::vector<float> out;

	for (size_t layer_idx = 0; layer_idx < configuration.size() - 1; layer_idx++) // �������� �� �����
	{
		out.resize(configuration[layer_idx + 1], 0.0);
		for (size_t to_idx = 0; to_idx < configuration[layer_idx + 1]; to_idx++)
		{
			for (size_t from_idx = 0; from_idx < configuration[layer_idx]; from_idx++)
			{
				out[to_idx] += weights[layer_idx][from_idx][to_idx] * prev_out[from_idx]; // ������� ����� �����
			}
			out[to_idx] = Activation(out[to_idx]);
		}
		prev_out = out; //������ ����� �� ������� ����
	}

	return out;

}
std::shared_ptr<ANN::ANeuralNetwork> ANN::CreateNeuralNetwork(std::vector<size_t>& conf, ANN::ANeuralNetwork::ActivationType aType, float scale)
{
	return std::make_shared<NeuralNetworkImpl>(conf, aType, scale);
}
float ANN::BackPropTraining(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	std::vector<std::vector<float>> & inputs,
	std::vector<std::vector<float>> & outputs,
	int maxIters,
	float eps,
	float speed,
	bool std_dump)
{
	float error;
	int iter = 0;
	ann->RandomInit();
	if (inputs.size() != outputs.size()) throw std::runtime_error("Number of input elements is not equal to output");
	do
	{
		error = 0;
		for (size_t sample_idx = 0; sample_idx < inputs.size(); sample_idx++)
		{
			error += BackPropTrainingIteration(ann, inputs[sample_idx], outputs[sample_idx], speed);
		}
		error = sqrt(error);

		iter++;
		if (std_dump && iter % 100 == 0){ std::cout << iter << "/" << maxIters << "\t Error: " << error << std::endl; }
		if (error < eps) ann->is_trained = true;
	} while (error > eps && iter < maxIters);

	return error;
}

float ANN::BackPropTrainingIteration(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	const std::vector<float>& input,
	const std::vector<float>& output,
	float speed)
{
	float error = 0.0f;

	std::vector< std::vector <float>> outs(ann->configuration.size());
	outs[0] = input;
	for (size_t layer_idx = 0; layer_idx < ann->configuration.size() - 1; layer_idx++){
		outs[layer_idx + 1].resize(ann->configuration[layer_idx + 1], 0);
		for (size_t to_idx = 0; to_idx < ann->configuration[layer_idx + 1]; to_idx++){
			for (size_t from_idx = 0; from_idx < ann->configuration[layer_idx]; from_idx++){
				outs[layer_idx + 1][to_idx] += ann->weights[layer_idx][from_idx][to_idx] * outs[layer_idx][from_idx];
			}
			outs[layer_idx + 1][to_idx] = ann->Activation(outs[layer_idx + 1][to_idx]);
		}
	}

	std::vector < std::vector < float>> sigmas(ann->configuration.size());
	sigmas.back().resize(outs.back().size());
	for (size_t id = 0; id < output.size(); id++)
	{
		sigmas.back()[id] = (output[id] - outs.back()[id])*ann->ActivationDerivative(outs.back()[id]);
		error += (outs.back()[id] - output[id])*(outs.back()[id] - output[id]);
	}

	std::vector<std::vector<std::vector<float>>> dw(ann->configuration.size() - 1);

	for (size_t layer_idx = ann->configuration.size() - 2; layer_idx + 1 != 0; layer_idx--)
	{
		dw[layer_idx].resize(ann->weights[layer_idx].size());
		sigmas[layer_idx].resize(ann->configuration[layer_idx], 0.0f);

		for (size_t from_idx = 0; from_idx < ann->configuration[layer_idx]; from_idx++)
		{
			for (size_t to_idx = 0; to_idx < ann->configuration[layer_idx + 1]; to_idx++){
				sigmas[layer_idx][from_idx] += sigmas[layer_idx + 1][to_idx] * ann->weights[layer_idx][from_idx][to_idx];
			}
			sigmas[layer_idx][from_idx] *= ann->ActivationDerivative(outs[layer_idx][from_idx]);
			dw[layer_idx][from_idx].resize(ann->weights[layer_idx][from_idx].size());
			for (size_t to_idx = 0; to_idx < ann->configuration[layer_idx + 1]; to_idx++){
				dw[layer_idx][from_idx][to_idx] = speed * sigmas[layer_idx + 1][to_idx] * outs[layer_idx][from_idx];
				ann->weights[layer_idx][from_idx][to_idx] += dw[layer_idx][from_idx][to_idx];
			}
		}
	}

	return error;
}