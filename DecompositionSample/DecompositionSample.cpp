#include <iostream>
#include <FeatureExtraction.h>
#include "Visualisation.h"
using namespace fe;
using namespace cv;
using namespace std;

int main() 
{
	cout << "hello world!" << endl;
	cout << fe::GetTestString().c_str() << endl;
	 
	auto blob_proc = CreateBlobProcessor();
	cout << blob_proc->GetType() << endl;
	auto image = imread("numbers.png", IMREAD_GRAYSCALE);
	imshow("src", image);
	auto blobs = blob_proc->DetectBlobs(image);
	auto polynomials = CreatePolynomialManager();
	cout << polynomials->GetType() << endl;
	polynomials->InitBasis(12, 30);
	ShowPolynomials("polynomials", polynomials->GetBasis());
	auto normalize_blobs = blob_proc->NormalizeBlobs(blobs, 30);
	for (auto& blobs : normalize_blobs)
	{
		auto dec = polynomials->Decompose(blobs);
		auto rec = polynomials->Recovery(dec);
		Show64FC1Mat("rec", rec);
		waitKey(0);
	}
	
	waitKey(0);
	return 0;
}